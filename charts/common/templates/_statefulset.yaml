{{- define "common.statefulset" }}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "common.fullname" . }}
  labels:
  {{- include "common.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  {{- with .Values.updateStrategy }}
  updateStrategy:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
  {{- include "common.selectorLabels" . | nindent 6 }}
  serviceName: {{ include "common.fullname" . }}-headless
  podManagementPolicy: {{ .Values.podManagementPolicy }}
  template:
    metadata:
  {{- if or .Values.podAnnotations .Values.secretEnvVars }}
      annotations:
    {{- if .Values.secrets }}
        checksum/secrets: {{ include "common.secrets" . | sha256sum }}
    {{- end }}
    {{- if .Values.configMaps }}
        checksum/configMaps: {{ include "common.configMaps" . | sha256sum }}
    {{- end }}
    {{- with .Values.podAnnotations }}
      {{- toYaml . | nindent 8 }}
    {{- end }}
  {{- end }}
      labels:
  {{- include "common.selectorLabels" . | nindent 8 }}
    spec:
  {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
    {{- toYaml . | nindent 8 }}
  {{- end }}
      serviceAccountName: {{ include "common.serviceAccountName" . }}
  {{- with .Values.nodeSelector }}
      nodeSelector:
    {{- toYaml . | nindent 8 }}
  {{- end }}
  {{- with .Values.affinity }}
      affinity:
    {{- toYaml . | nindent 8 }}
  {{- end }}
  {{- with .Values.tolerations }}
      tolerations:
    {{- toYaml . | nindent 8 }}
  {{- end }}
  {{- if .Values.podSecurityContext.enabled }}
      securityContext:
    {{- omit .Values.podSecurityContext "enabled" | toYaml | nindent 8 }}
  {{- end }}
  {{- with .Values.initContainers }}
      initContainers:
    {{- toYaml . | nindent 8 }}
  {{- end }}
      containers:
        - name: {{ include "common.name" . }}
          image: {{ .Values.image.repository }}:{{ include "common.appVersion" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
  {{- if .Values.containerSecurityContext.enabled }}
          securityContext:
    {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
  {{- end }}
  {{- with .Values.port }}
          ports:
            - name: {{ include "common.portName" . }}
              containerPort: {{ .number }}
              protocol: {{ include "common.portProtocol" . }}
  {{- end }}
  {{- if .Values.diagnosticMode.enabled }}
          command: {{ toJson .Values.diagnosticMode.command }}
          args: {{ toJson .Values.diagnosticMode.args }}
  {{- end }}
  {{- if .Values.resources.enabled }}
          resources:
    {{- omit .Values.resources "enabled" | toYaml | nindent 12 }}
  {{- end }}
  {{- if .Values.startupProbe.enabled }}
          startupProbe:
    {{- omit .Values.startupProbe "enabled" | toYaml | nindent 12 }}
  {{- end }}
  {{- if .Values.livenessProbe.enabled }}
          livenessProbe:
    {{- omit .Values.livenessProbe "enabled" | toYaml | nindent 12 }}
  {{- end }}
  {{- if .Values.readinessProbe.enabled }}
          readinessProbe:
    {{- omit .Values.readinessProbe "enabled" | toYaml | nindent 12 }}
  {{- end }}
  {{- if or .Values.envVars (eq .Values.language "java") }}
          env:
    {{- range $key, $val := omit .Values.envVars "SPRING_CLOUD_KUBERNETES_CONFIG_NAME" "SPRING_CLOUD_KUBERNETES_CONFIG_NAMESPACE" "JAVA_OPTS" }}
            - name: {{ $key }}
              value: {{ $val | quote }}
    {{- end }}
    {{- if eq .Values.language "java" }}
      {{- if hasKey .Values.envVars "JAVA_OPTS" }}
        {{- $javaOpts := get .Values.envVars "JAVA_OPTS" }}
            - name: JAVA_OPTS
              value: "-Dfile.encoding=UTF-8 -Djava.awt.headless=true -XX:+HeapDumpOnOutOfMemoryError -Djava.net.preferIPv4Stack=true {{ $javaOpts }}"
      {{- end }}
      {{- if or (kindIs "string" .Values.springConfig.yaml) (kindIs "string" .Values.springConfig.properties) }}
            - name: SPRING_CLOUD_KUBERNETES_CONFIG_NAME
              value: {{ include "common.springConfigName" . }}
            - name: SPRING_CLOUD_KUBERNETES_CONFIG_NAMESPACE
              value: {{ get .Values.envVars "SPRING_CLOUD_KUBERNETES_CONFIG_NAMESPACE" | default .Release.Namespace }}
      {{- end }}
    {{- end }}
  {{- end }}
  {{- with .Values.envFrom }}
          envFrom:
    {{- toYaml . | nindent 12 }}
  {{- end }}
  {{- with .Values.volumeMounts }}
          volumeMounts:
    {{- toYaml . | nindent 12 }}
  {{- end }}
  {{- with .Values.volumes }}
      volumes:
    {{- toYaml . | nindent 8 }}
  {{- end }}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "common.fullname" . }}-headless
  labels:
  {{- include "common.labels" . | nindent 4 }}
spec:
  clusterIP: None
  ports:
    - name: {{ include "common.portName" . }}
      port: {{ .Values.port.number }}
      targetPort: {{ .Values.port.number }}
      protocol: {{ include "common.portProtocol" . }}
  selector:
  {{- include "common.selectorLabels" . | nindent 4 }}
---
{{ end }}
